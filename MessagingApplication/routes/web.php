<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'CampaignController@index')->name('home');
Route::resource('campaign', 'CampaignController');
Route::get('start', ['as' => 'start', 'uses' => 'CampaignController@startAction']);
Route::get('stop', ['as' => 'stop', 'uses' => 'CampaignController@stopAction']);
Route::get('delete', ['as' => 'delete', 'uses' => 'CampaignController@deleteAction']);
Route::get('pause', ['as' => 'pause', 'uses' => 'CampaignController@pauseAction']);
Route::get('statusUpdate', ['as' => 'statusUpdate', 'uses' => 'CampaignController@statusUpdate']);
Route::get('responseUpdate', ['as' => 'responseUpdate', 'uses' => 'CampaignController@responseUpdate']);
Route::get('smsResponse', ['as' => 'smsResponse', 'uses' => 'CampaignController@smsResponse']);

