<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'campaigns';
    protected $fillable = ['campaign_name', 'sms_text', 'schedule_date', 'status', 'is_delete','response'];
}
