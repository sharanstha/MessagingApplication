<?php

namespace App\Http\Controllers;

use App\Campaign;
use http\Env\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;
use function MongoDB\BSON\fromJSON;
use function MongoDB\BSON\toJSON;
use Symfony\Component\HttpKernel\Client;
use Illuminate\Http\Resources\Json\Resource;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['campaigns'] = Campaign::all();
        return view('campaign.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('campaign.create');

    }

    //for redirect to edit page
    public function edit($id)
    {
        $data['campaign'] = Campaign::find($id);
        return view('campaign.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $data = array(
            'campaign_name' => $request->campaign_name,
            'sms_text' => $request->sms_text,
            'schedule_date' => $request->schedule_date,
        );

        Campaign::where('id', '=', $id)
            ->update($data);

        return Redirect::to('campaign');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = array(
            'campaign_name' => $request->campaign_name,
            'sms_text' => $request->sms_text,
            'schedule_date' => $request->schedule_date,
            'status' => 0,
            'is_delete' => 0,
        );

        $created = Campaign::create($arr);

        $campaign_id = $created->id;
        $file = $request->file('csv_file');
        $this->importFile($file, $campaign_id);

        return redirect('/campaign');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['campaign'] = Campaign::find($id);
        $data['numbers'] = DB::table('numbers')
            ->where('campaign_id', '=', $id)
            ->get()
            ->pluck('number');

        return view('campaign.detail', $data);
    }

    //to import excel file that contain phone numbers
    public function importFile($file, $campaign_id)
    {

        $path = $file->getRealPath();
        $data = Excel::load($path, function ($reader) {
        })->get();
        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $insert[] = [
                    'number' => $value->number,
                    'campaign_id' => $campaign_id,
                ];

            }
            if (!empty($insert)) {
                DB::table('numbers')->insert($insert);
                return 'data import successfully';
            }
        }

    }

//    for start button
    public function startAction(Request $request)
    {
        $campaign_id = Input::get('id');

        //phone_number form numbers table
        $phone_numbers = DB::table('numbers')
            ->where('campaign_id', '=', $campaign_id)
            ->get()
            ->pluck('number');

        //sms from campaign table of relavant id
        $sms_text = Campaign::where('id', '=', $campaign_id)
            ->get()
            ->pluck('sms_text');

        $phoneNumGroup = "";//initialize $phone number group
        for ($i = 0; $i < sizeof($phone_numbers); $i++) {
            $phone = $phone_numbers[$i];
            $phoneNumGroup = $phone . '+' . $phoneNumGroup;
        }
        $dataArr = array(
            'phoneNumGroup' => $phoneNumGroup,
            'sms_text' => $sms_text
        );
        return json_encode($dataArr);
    }

    //get response from server
    public function smsResponse(Request $request)
    {
        $url = Input::get('url');
        $file = file_get_contents($url);
        return $file;
    }

    //after clicking start button update status==1 :(1 means start)
    public function statusUpdate()
    {
        $campaign_id = Input::get('campaign_id');
        $data = array(
            'status' => 1
        );
        Campaign::where('id', '=', $campaign_id)
            ->update($data);
        return "status is sending";

    }

    //after getting response form server update response message
    public function responseUpdate()
    {
        $response = Input::get('response');
        $campaign_id = Input::get('campaign_id');
        $data = array(
            'response' => $response
        );
        Campaign::where('id', '=', $campaign_id)
            ->update($data);
        return "response is sending";

    }

    //status ==2 means stop
    public function stopAction()
    {
        $campaign_id = Input::get('id');;
        $data = array(
            'status' => 2
        );
        Campaign::where('id', '=', $campaign_id)
            ->update($data);
        return "status is stopping";

    }

//    is_delete==1 means not to display in index page but data will be in database
    public function deleteAction()
    {
        $campaign_id = Input::get('id');;
        $data = array(
            'is_delete' => 1
        );
        Campaign::where('id', '=', $campaign_id)
            ->update($data);
        return "deleting...";

    }

//    status==3 means pause
    public function pauseAction()
    {
        $campaign_id = Input::get('id');;
        $data = array(
            'status' => 3
        );
        Campaign::where('id', '=', $campaign_id)
            ->update($data);
        return "pausing...";
    }


}
