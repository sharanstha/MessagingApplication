var phoneNumGroup = 0;
var sms_text = null;

function start(id) {
    $('#' + id).html('running..');
    $.ajax({
        url: 'start',//calling startAction from CampaignController
        type: "GET",
        data: {id: id},
        datatype: 'json',
        beforeSend: function (jqXHR, options) {
            setTimeout(function () {
                // null beforeSend to prevent recursive ajax call
                $.ajax($.extend(options, {beforeSend: $.noop}));
            }, 4000);
            return false;
        },
        success: function (data) {
            var d = JSON.parse(data);
            sendSMS(d.phoneNumGroup, d.sms_text, id);
            $.ajax({
                url: 'statusUpdate', //calling statusUpdate from CampaignController
                type: "GET",
                data: {campaign_id: id},
                success: function (data) {
                    $('#' + id).html('');
                    $('#' + id).html('completed..');
                    $("#start" + id).prop('disabled', true);
                }
            });

        }
    });
}


function sendSMS(phoneNumGroup, sms_text, campaign_id) {
    let uname = 'unifun';
    let pass = 'unifun123';
    let host = '192.168.56.101';//change ip to localhost
    let url = "http://" + host + ":13013/cgi-bin/sendsms?username=" + uname + "&password=" + pass + "&from=100&to=" + phoneNumGroup + "&text=&dlr-mask=31&drl-url=http%3A%2F%2Flocalhost%2Fdelivery_response.php%3FCampaignID%3D2%26MSISDN%3D9855684222%26Response%3D%25d";
    $.ajax({
        url: 'smsResponse',//calling smsResponse from CampaignController
        type: "GET",
        data: {url: url},
        datatype: 'json',
        success: function (data) {
            $.ajax({
                url: 'responseUpdate',//calling responseUpdate from CampaignController
                type: "GET",
                data: {response: data, campaign_id: campaign_id},
                datatype: 'json',
                success: function (data) {
                    alert(data);
                }
            });

            alert(data);
        }
    });
}

//
function stop(id) {
    $('#' + id).html('');
    $('#' + id).html('stopping..');

    $.ajax({
        url: 'stop',//calling stopAction from CampaignController
        type: "GET",
        data: {id: id},
        datatype: 'json',
        success: function () {

            $('#' + id).html('');
            $('#' + id).html('stopped..');
            // $("#start" + id).prop('disabled', true);
            // $("#stop" + id).prop('disabled', true);
            // $("#pause" + id).prop('disabled', true);
            // $("#delete" + id).prop('disabled', true);
            window.location.reload();
        }
    });


}

function deleteMark(id) {
    $('#' + id).html('');
    $('#' + id).html('deleting..');
    $.ajax({
        url: 'delete',//calling deleteAction from CampaignController
        type: "GET",
        data: {id: id},
        datatype: 'json',
        success: function (data) {
            window.location.reload();
        }
    });
}

function pause(id) {
    $('#' + id).html('');
    $('#' + id).html('pausing..');
    $.ajax({
        url: 'pause',//calling pauseAction from CampaignController
        type: "GET",
        data: {id: id},
        datatype: 'json',
        success: function (data) {
            $('#' + id).html('');
            $('#' + id).html('paused..');
            window.location.reload();
        }
    });

}

$('[data-toggle="popover"]').popover({
    placement: 'top',
    trigger: 'hover'
});
