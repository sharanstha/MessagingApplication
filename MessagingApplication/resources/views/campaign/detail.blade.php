@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{route('campaign.index')}}" class="btn btn-success pull-right">BACK</a>

                    </div>
                    <div class="card-body">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h2>Campaign Detail</h2></div>
                            <div class="panel-heading"><h2>SN:{{$campaign->id}}</h2></div>
                            <table class="table">
                                <th>
                                <td>
                                    <label class="col-md-4 control-label"><b>Campaign Name:</b></label>
                                    <p>{{$campaign->campaign_name}}</p>
                                </td>
                                <td>
                                    <b>NUMBERS:</b>
                                    @forelse($numbers as $n)
                                        <span>{{$n}}</span></br>
                                    @empty
                                    @endforelse
                                </td>
                                </th>
                            </table>
                            <table class="table">

                                <tr>
                                    <label class="col-md-4 control-label"><b>SMS text:</b></label>
                                    <textarea id="sms_text" name="sms_text" class="form-control" readonly>
                                {{$campaign->sms_text}}
                                    </textarea>
                                </tr>
                                <tr>
                                    <label class="col-md-4 control-label"><b>Send date/time:</b></label>
                                    <p>{{$campaign->created_at}}</p>
                                </tr>
                                <tr>
                                    <label class="col-md-4 control-label"><b>Send status:</b></label>
                                    @if($campaign->status==1)
                                        <p>completed...</p>
                                    @elseif($campaign->status==2)
                                        <p>stopped...</p>
                                    @elseif($campaign->status==3)
                                        <p>paused...</p>
                                    @elseif($campaign->is_delete==1)
                                        <p>deleted...</p>
                                    @else
                                        <p>pending...</p>
                                    @endif
                                </tr>
                                <tr>
                                    <label class="col-md-4 control-label"><b>server response:</b></label>
                                    <p>{{$campaign->response}}</p>
                                </tr>
                                <tr>
                                    <label class="col-md-4 control-label"><b>Schedule date:</b></label>
                                    <p>{{$campaign->schedule_date}}</p>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


