@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Edit Campaign</h2></div>

                    <div class="panel-body">
                        {{--{{ route('import_parse') }}--}}
                        {{ Form::model($campaign, array('method' => 'patch', 'route' => array('campaign.update', $campaign->id))) }}

                            <div class="col-md-6">
                                <label for="campaign_name" class="col-md-4 control-label">Campaign Name:</label>
                                {!! Form::text('campaign_name', $value = null, ['class'=>'form-control','required']) !!}

                            </div>
                            <div class="col-md-6">
                                <label for="sms_text" class="col-md-4 control-label">SMS Text:</label>
                                {!! Form::textarea('sms_text', $value = null, ['class'=>'form-control','required']) !!}

                            </div>

                            <div class="col-md-6">
                                <label for="schedule_date" class="col-md-4 control-label">schedule date:</label>
                                {!! Form::text('schedule_date', $value = null, ['class'=>'form-control','required']) !!}

                            </div>

                            {{--<div class="col-md-6">--}}
                            {{--<label for="csv_file" class="col-md-4 control-label">Mobile Number:</label>--}}
                            {{--<input id="csv_file" type="file" class="form-control" name="csv_file" required>--}}

                            {{--@if ($errors->has('csv_file'))--}}
                            {{--<span class="help-block">--}}
                            {{--<strong>{{ $errors->first('csv_file') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--</div>--}}

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    submit
                                </button>
                                <a href="{{route('campaign.index')}}" type="submit" class="btn btn-danger">
                                    back
                                </a>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


