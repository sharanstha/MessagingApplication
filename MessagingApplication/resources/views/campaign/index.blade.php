@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <a href="{{route('campaign.create')}}" class="btn btn-success pull-right">ADD Campaign</a>

                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered">
                            <thead>

                            <tr>
                                <th>campaign name</th>
                                <th>sms text</th>
                                <th>created date</th>
                                <th>schedule date</th>
                                <th>status</th>
                                <th>sta
                                    top
                                </th>
                                <th>pause | delete</th>
                                <th>report | edit</th>

                            </tr>
                            </thead>

                            <tbody>

                            @forelse($campaigns as $campaign)
                                @if($campaign->is_delete<>1)
                                    <tr>

                                        <td>{{$campaign->campaign_name}}</td>

                                        <td data-toggle="popover" data-content="{{$campaign->sms_text}}">
                                            {{substr($campaign->sms_text,-10)}}</td>

                                        <td>{{$campaign->created_at}}</td>

                                        <td>{{$campaign->schedule_date}}</td>
                                        @if($campaign->status==1)
                                            <td id="{{$campaign->id}}">completed....</td>
                                        @elseif($campaign->status==2)
                                            <td id="{{$campaign->id}}">stopped...</td>

                                        @elseif($campaign->status==3)
                                            <td id="{{$campaign->id}}">paused...</td>
                                        @else
                                            <td id="{{$campaign->id}}">pending...</td>
                                        @endif

                                        <td>
                                            <button href="" id="start{{$campaign->id}}"
                                                    onclick="start({{$campaign->id}})"
                                                    class="btn btn-info btn-xs {{$campaign->id}}"><label
                                                        class="glyphicon glyphicon-pencil"></label>start
                                            </button>
                                            <br/><br/>
                                            <button href="" id="stop{{$campaign->id}}" onclick="stop({{$campaign->id}})"
                                                    class="btn btn-success btn-xs {{$campaign->id}}"><label
                                                        class="glyphicon glyphicon-eye-open"></label>stop
                                            </button>
                                        </td>
                                        <td>
                                            <button href="" id="pause{{$campaign->id}}"
                                                    onclick="pause({{$campaign->id}});"
                                                    class="btn btn-info btn-xs {{$campaign->id}}"><label
                                                        class="glyphicon glyphicon-pencil"></label>pause
                                            </button>
                                            <br/><br/>
                                            <button href="" id="delete{{$campaign->id}}"
                                                    onclick="deleteMark({{$campaign->id}})"
                                                    class="btn btn-danger btn-xs {{$campaign->id}}"><label
                                                        class="glyphicon glyphicon-pencil"></label>delete
                                            </button>

                                        </td>
                                        <td>
                                            <a href="{{route('campaign.show', array($campaign->id))}}"
                                               class="btn btn-primary pull-right">report</a><br/><br/>
                                            <a href="{{route('campaign.edit', array($campaign->id))}}"
                                               class="btn btn-primary pull-right">edit</a>
                                        </td>

                                    </tr>
                                    @if($campaign->status==1)
                                        <script>
                                            $("#start{{$campaign->id}}").prop('disabled', true);
                                        </script
                                    @elseif($campaign->status==2)
                                        <script>
                                            {{--alert("#start{{$campaign->id}}");--}}
                                            $(".{{$campaign->id}}").prop('disabled', true);
                                        </script>
                                    @endif
                                @endif
                            @empty
                                <tr>
                                    <td colspan="15">
                                        <p class="text-danger text-center"><b>No data found !</b></p>
                                    </td>

                                </tr>
                            @endforelse

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection
