
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Add Campaign</h2></div>

                    <div class="panel-body">
                        {{--{{ route('import_parse') }}--}}
                        <form class="form-horizontal" method="POST" action="/import" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">


                                {{--<div class="col-md-6">--}}
                                    {{--<label for="campaign_name" class="col-md-4 control-label">Campaign Name:</label>--}}
                                    {{--<input id="campaign_name" type="text" class="form-control" name="campaign_name" required>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<label for="sms_text" class="col-md-4 control-label">SMS Text:</label>--}}
                                    {{--<textarea id="sms_text" name="sms_text" class="form-control" required>--}}
                                    {{--</textarea>--}}
                                {{--</div>--}}


                                <div class="col-md-6">
                                    <label for="csv_file" class="col-md-4 control-label">Mobile Number:</label>
                                    <input id="csv_file" type="file" class="form-control" name="csv_file" required>

                                    @if ($errors->has('csv_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('csv_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-6 col-md-offset-4">--}}
                                    {{--<div class="checkbox">--}}
                                        {{--<label>--}}
                                            {{--<input type="checkbox" name="header" checked> File contains header row?--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


