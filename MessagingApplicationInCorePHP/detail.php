<?php
session_start();
if (!isset($_SESSION['sess_user_id'])) {
    header('location:login.php');
}
include('config.php');
?>

<?php require('header.php'); ?>

<div class="container">
    <div class="row">
        <h1 align="right">
            <span><a href="log_out.php"><b style="color:black">LOGOUT</b></a></span>
        </h1>
        <h3 align="center"><a href="dashboard.php">GoBack</a></h3>


        <h1 align="center">REPORT:</h1>

        <table align="center" border="1">
            <tr>
                <th>numbers:</th>

            </tr>
            <?php
            //            $query = "select * from campaigns c inner join numbers n on c.id=n.campaign_id where c.id=" . $_GET['id'];
            $queryForNumber = "select * from numbers where campaign_id=" . $_GET['id'];
            $result = mysqli_query($connect, $queryForNumber);
            while ($data = mysqli_fetch_array($result)) {
                ?>
                <tr>
                    <td><?php echo $data['number']; ?></td>
                </tr>
            <?php } ?>
        </table>

        <table align="center" border="1">
            <tr>
                <th>campaign name:</th>
                <th>sms text:</th>
                <th>schedule_date:</th>
                <th>status:</th>
                <th>response:</th>
                <th>send date:</th>
            </tr>
            <?php
            $queryForCampaign = "select * from campaigns where id=" . $_GET['id'];
            $resultCampaign = mysqli_query($connect, $queryForCampaign);

            while ($dataCampaign = mysqli_fetch_array($resultCampaign)) {
                ?>
                <tr>
                    <td><?php echo $dataCampaign['campaign_name']; ?></td>
                    <td><textarea><?php echo $dataCampaign['sms_text']; ?></textarea></td>
                    <td><?php echo $dataCampaign['schedule_date']; ?></td>
                    <td id="<?php echo $dataCampaign['id']; ?>">
                        <?php
                        if ($dataCampaign['status'] == 1) { ?>
                            completed...
                        <?php } else if ($dataCampaign['status'] == 2) {
                            ?>
                            stopped..
                            <?php
                        } else if ($dataCampaign['status'] == 3) {
                            ?>
                            paused..
                            <?php
                        } else {
                            ?>
                            pending...
                            <?php
                        } ?>
                    </td>
                    <td><?php echo $dataCampaign['response']; ?></td>
                    <td><?php echo $dataCampaign['created_at']; ?></td>

                </tr>

            <?php } ?>


        </table>

        <?php include('footer.php'); ?>

    </div>
</div>


</body>
</html>