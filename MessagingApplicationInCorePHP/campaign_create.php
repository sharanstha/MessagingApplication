<?php
//for campaign create-->
session_start();
if (!isset($_SESSION['sess_user_id'])) {
    header('location:login.php');
}
include('config.php');

if (isset($_POST['campaign_create'])) {
    include 'config.php';
    $cname = $_POST['campaign_name'];
    $sms = $_POST['sms_text'];
    $date = $_POST['schedule_date'];
    $phone = $_POST['file'];
    $created_date = date('y-m-d h:i');

    $query = "insert into campaigns values('','$cname','$sms','$date','','','','$date','')";
    mysqli_query($connect, $query);

    $queryForCampaignId = "select max(id) as id from campaigns";
    $result = mysqli_query($connect, $queryForCampaignId);
    while ($data = mysqli_fetch_array($result)) {
        $campaignId = $data['id'];
    }

    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    } else {
        if (file_exists("file/" . $_FILES["file"]["name"])) {
            echo $_FILES["file"]["name"] . " already exists. ";
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"],
                "file/" . $_FILES["file"]["name"]);
            echo "Stored in: " . "file/" . $_FILES["file"]["name"]; //<- This is it
        }
    }
    $filename = $_SERVER["DOCUMENT_ROOT"] . '/MessagingApplicationInCorePHP/file/' . $_FILES["file"]["name"];
    //    if ($_POST["file"]["size"] > 0) {
    $file = fopen($filename, "r");
    while (($emapData = fgetcsv($file, 1000, ",")) !== FALSE) {
        //It wiil insert a row to our subject table from our csv file`
        $sql = "INSERT into numbers values('',$emapData[0],$campaignId)";
        //we are using mysqli_query function. it returns a resource on true else False on error
        $result = mysqli_query($connect, $sql);
        if (!$result) {
            echo "<script type=\"text/javascript\">
                                alert(\"Invalid File:Please Upload CSV File.\");
                                window.location = \"dashboard.php\"
                            </script>";
        }

    }
    fclose($file);
    //throws a message if data successfully imported to mysql database from excel file
    echo "<script type=\"text/javascript\">
                            alert(\"CSV File has been successfully Imported.\");
                            window.location = \"dashboard.php\"
                        </script>";

    header("location:dashboard.php?campaignMessage=successfully created...");

}
?>
<?php require('header.php'); ?>
<div class="container">
    <div class="row">
        <form method="post" name="campaignForm" action="campaign_create.php" enctype="multipart/form-data">
            <table align="center" border="1">
                <tr>
                    <td><input required="required" type="text"
                               placeholder="please enter campaign name"
                               class="form-control"
                               name="campaign_name"/></td>
                </tr>
                <tr>
                    <td>
                                        <textarea required="required" placeholder="sms text" id="sms_text"
                                                  class="form-control" name="sms_text"></textarea></td>
                </tr>
                <tr>
                    <td><input required="required" id="schedule_date" type="date"
                               placeholder="please enter schedule_date"
                               class="form-control" name="schedule_date"/></td>
                </tr>
                <tr>
                    <td><input required="required" id="csv_file" type="file"
                               placeholder="csv_file" class="form-control" name="file"/></td>

                </tr>
                <tr>
                    <td><input type="submit" value="Add Campaign" name="campaign_create"/><a
                                href="dashboard.php">GoBack</a>
                    </td>
                </tr>
            </table>
        </form>
        <?php require('footer.php'); ?>
    </div>
</div>
<body>
</html>