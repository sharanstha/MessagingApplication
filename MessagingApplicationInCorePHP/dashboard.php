<?php
session_start();
if (!isset($_SESSION['sess_user_id'])) {
    header('location:login.php');
}
include('config.php');

require_once 'controller.php';
?>

<?php require('header.php'); ?>

<div class="container">
    <div class="row">
        <h1 align="right">
            <span><a href="log_out.php"><b style="color:black">LOGOUT</b></a></span>
        </h1>


        <h1 align="center">DASHBOARD:</h1>
        <h3 align="center"><a href="campaign_create.php">Add campaign</a></h3>
        <h5 align="center" style="color:green"><?php if (isset($_GET['campaignMessage'])) {
                echo $_GET['campaignMessage'];
            } ?></h5>
        <table align="center" border="1">
            <tr>
                <th>campaign name:</th>
                <th>sms text:</th>
                <th>schedule date:</th>
                <th>created at:</th>
                <th>status:</th>
                <th colspan="4">action:</th>
                <th colspan="2">reports:</th>
            </tr>
            <?php
            $query = "select * from campaigns";
            $result = mysqli_query($connect, $query);
            while ($data = mysqli_fetch_array($result)) {
            if ($data['is_delete'] <> 1) {
                ?>
                <tr>
                    <td><?php echo $data['campaign_name']; ?></td>
                    <td><a data-toggle="popover" href=""
                           data-content="<?php echo $data['sms_text']; ?>"><?php echo substr($data['sms_text'], 0, 10); ?>
                        </a></td>
                    <td><?php echo $data['created_at']; ?></td>
                    <td><?php echo $data['schedule_date']; ?></td>
                    <td id="<?php echo $data['id']; ?>">
                        <?php
                        if ($data['status'] == 1) { ?>
                            completed...
                        <?php } else if ($data['status'] == 2) {
                            ?>
                            stopped..
                            <?php
                        } else if ($data['status'] == 3) {
                            ?>
                            paused..
                            <?php
                        } else {
                            ?>
                            pending...
                            <?php
                        } ?>
                    </td>
                    <td>
                        <button onclick="start(<?php echo $data['id']; ?>)" id="start<?php echo $data['id']; ?>"
                                href="">start
                        </button>
                    </td>
                    <td>
                        <button onclick="stop(<?php echo $data['id']; ?>)" id="stop<?php echo $data['id']; ?>"
                                href="">stop
                        </button>
                    </td>
                    <td>
                        <button onclick="pause(<?php echo $data['id']; ?>)" id="pause<?php echo $data['id']; ?>"
                                href="">pause
                        </button>
                    </td>
                    <td>
                        <button onclick="deleteMark(<?php echo $data['id']; ?>)"
                                id="delete<?php echo $data['id']; ?>"
                                href="">delete
                        </button>
                    </td>
                    <td><a href="detail.php?id=<?php echo $data['id']; ?>">report</a></td>

                    <?php
                    ?>
                </tr>
            <?php }
            if ($data['status'] == 1) {
            ?>
                <script>
                    $("#start" + <?php  echo $data['id'];?>).prop('disabled', true);
                </script>
            <?php
            } else if ($data['status'] == 2) {
            ?>
                <script>
                    $("#start" + <?php  echo $data['id'];?>).prop('disabled', true);
                    $("#stop" + <?php  echo $data['id'];?>).prop('disabled', true);
                    $("#pause" + <?php  echo $data['id'];?>).prop('disabled', true);
                    $("#delete" + <?php  echo $data['id'];?>).prop('disabled', true);
                </script>
                <?php
            }
            }
            ?>

        </table>


        <?php include('footer.php'); ?>

    </div>
</div>


</body>
</html>


<script type=text/javascript>

    function start(id) {

        $('#' + id).html('');
        $('#' + id).html('running..');
        $.ajax({
            type: "GET",
            data: {id: id},
            url: 'handler/start_handler.php',
            dataType: "html",
            async: false,
            beforeSend: function () {
                $("#stop" + id).prop('disabled', false);
                $("#pause" + id).prop('disabled', false);
                setTimeout(5000);
            },
            success: function (data) {
                var d = JSON.parse(data);
                sendSMS(d.phoneNumGroup, d.sms_text, id);
                $.ajax({
                    url: 'handler/start_status_update.php', //calling statusUpdate from CampaignController
                    type: "GET",
                    data: {campaign_id: id},
                    success: function (data) {

                        $('#' + id).html('');
                        $('#' + id).html('completed..');
                        $("#start" + id).prop('disabled', true);
                    }
                });

            }
        });
    }

    function sendSMS(phoneNumGroup, sms_text, campaign_id) {
        let uname = 'unifun';
        let pass = 'unifun123';
        let host = '192.168.56.101';//change ip to localhost
        let url = "http://" + host + ":13013/cgi-bin/sendsms?username=" + uname + "&password=" + pass + "&from=100&to=" + phoneNumGroup + "&text=&dlr-mask=31&drl-url=http%3A%2F%2Flocalhost%2Fdelivery_response.php%3FCampaignID%3D2%26MSISDN%3D9855684222%26Response%3D%25d";
        $.ajax({
            url: 'handler/sms_handler.php',//calling smsResponse from CampaignController
            type: "GET",
            data: {url: url},
            datatype: 'html',
            async: false,
            success: function (data) {
                $.ajax({
                    url: 'handler/sms_response_update.php',//calling responseUpdate from CampaignController
                    type: "GET",
                    data: {response: data, campaign_id: campaign_id},
                    datatype: 'html',
                    async: false,
                    success: function (data) {
                    }
                });
            }
        });
    }

    //
    function stop(id) {
        $('#' + id).html('');
        $('#' + id).html('stopping..');

        $.ajax({
            url: 'handler/stop_handler.php',//calling stopAction from CampaignController
            type: "GET",
            data: {campaign_id: id},
            datatype: 'json',
            success: function () {
                $('#' + id).html('');
                $('#' + id).html('stopped..');
                $("#start" + id).prop('disabled', true);
                $("#stop" + id).prop('disabled', true);
                $("#pause" + id).prop('disabled', true);
                $("#delete" + id).prop('disabled', true);
            }
        });


    }

    function deleteMark(id) {
        $('#' + id).html('');
        $('#' + id).html('deleting..');
        $.ajax({
            url: 'handler/delete_handler.php',//calling deleteAction from CampaignController
            type: "GET",
            data: {campaign_id: id},
            datatype: 'json',
            success: function (data) {
            }
        });
    }

    function pause(id) {
        $('#' + id).html('');
        $('#' + id).html('pausing..');
        $.ajax({
            url: 'handler/pause_handler.php',//calling pauseAction from CampaignController
            type: "GET",
            data: {campaign_id: id},
            datatype: 'json',
            success: function (data) {
                $('#' + id).html('');
                $('#' + id).html('paused..');
            }
        });

    }

    $('[data-toggle="popover"]').popover({
        placement: 'bottom',
        trigger: 'hover'
    });
</script>
