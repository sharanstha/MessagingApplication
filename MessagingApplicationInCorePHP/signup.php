<?php include('header.php'); ?>
<?php
if (isset($_POST['signup'])) {
    include 'config.php';
    $fname = $_POST['fname'];
    $email = $_POST['email'];
    $pword = md5($_POST['pword']);
    $cpword = md5($_POST['cpword']);

    $queryfremail = "select email from users where email='$email'";
    $result = mysqli_query($connect, $queryfremail);
    if (mysqli_num_rows($result) > 0) {
        header("location:signup.php?msgfr_email=this email is already in use:please re-signup");
    } else {
        if ($pword == $cpword) {
            $query = "insert into users values('','$fname','$email','$pword')";
            mysqli_query($connect, $query);
            header("location:signup.php?msgfr_email=email valid");
            header("Location:signup.php?msg=signup successfully");

        } else {
            header("Location:signup.php?msgfrpw= password not matching");
        }
    }

}
?>
<div class="container">
    <div class="row">

        <div class="lgmsg">
            <h3 align="center" style="color:white">Sign up</h3>
            <h5 align="center" style="color:green"><?php if (isset($_GET['msg'])) {
                    echo $_GET['msg'];
                } ?></h5>
            <h5 align="center" style="color:green"><?php if (isset($_GET['msgfr_email'])) {
                    echo $_GET['msgfr_email'];
                } ?></h5>
            <h5 align="center" style="color:green"><?php if (isset($_GET['msgfrpw'])) {
                    echo $_GET['msgfrpw'];
                } ?></h5>
        </div>

        <form method="post" name="myform" action="signup.php">
            <table align="center" border="1">
                <tr>
                    <td><input required="required" type="text" placeholder="please enter full name" class="form-control"
                               name="fname"/></td>
                </tr>
                <tr>
                    <td><input required="required" type="email" placeholder="please enter Email" id="email"
                               class="form-control" name="email"/></td>
                </tr>
                <tr>
                    <td><input required="required" id="password" type="password" placeholder="please enter password"
                               onchange="validpword()" class="form-control" name="pword"/></td>
                </tr>
                <tr>
                    <td><input required="required" id="confirm_password" onchange="check()" type="password"
                               placeholder="please re-enter password" class="form-control" name="cpword"/></td>

                </tr>
                <tr>
                    <td id="message"></td>
                </tr>
                <tr>
                    <td><input type="submit" value="signup" name="signup"/></td>
                </tr>
                <tr>
                    <td><label>for login</label>
                        <a href="login.php">click here?</a></td>
                </tr>
            </table>
        </form>

        <?php include('footer.php'); ?>
    </div>
</div>
</body>
</html>
<script type="text/javascript">

    function check() {
        if (document.getElementById('password').value === document.getElementById('confirm_password').value) {
            document.getElementById('message').innerHTML = "password matched";
        } else {
            document.getElementById('message').innerHTML = "password is not matching";
            document.getElementById('confirm_password').focus();
        }
    }

    function validpword() {
        var pword = document.forms["myform"]["pword"].value;
        if (pword.length < 6) {
            alert("please enter password more than 6 letters");
            document.getElementById('password').focus();

        }
    }

</script>